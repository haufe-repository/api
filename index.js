'use strict'

require('dotenv').config()

const mongoose = require('mongoose')
const app = require('./app')

const port = process.env.PORT || 8978

const connection = `mongodb://localhost:27017/${process.env.DB_NAME}`

mongoose.Promise = global.Promise

mongoose.connect(connection, (err, res) => {
    if (err) {
        throw err
    } else {
        const server = app.listen(port, function () {
            console.log(`Server load on port: ${port}`)
        })
    }
})
