'use strict'

const express = require('express')
const helmet = require('helmet'); // https://www.npmjs.com/package/helmet
const xss = require('xss-clean'); // https://www.npmjs.com/package/xss-clean
const mongoSanitize = require('express-mongo-sanitize'); // https://www.npmjs.com/package/express-mongo-sanitize
const cors = require('cors')

const Account = require('./components/account/accountApi')
const Character = require('./components/character/characterApi')
const Favorite = require('./components/favorite/favoriteApi')

const app = express()
app.use(helmet({ frameguard: false }))
app.use(mongoSanitize())
app.use(xss())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const corsOptionsDelegate = function (req, callback) {
    callback(null, {
        methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Authorization', 'X-API-KEY', 'Origin', 'X-Request-With', 'Content-Type', 'Accept']
    })
}

app.use(cors(corsOptionsDelegate))

app.use(Account)
app.use(Character)
app.use(Favorite)

app.get('*', function (req, res, next) {
    return res.status(200).send({
        platform: 'Haufe Api'
    })
})

module.exports = app
