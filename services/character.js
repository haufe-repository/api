'use strict'

const Request = require('./../utils/request')

function list(params) {
    return Request({
        url: '/character',
        method: 'GET',
        params: params
    })
}

function detail(id) {
    return Request({
        url: '/character/' + id,
        method: 'GET'
    })
}

module.exports = {
    list,
    detail
}
  
