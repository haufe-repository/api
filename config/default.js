'use strict'

const DEFAULT = {
    endpoint: {
        api: process.env.API_HOST
    },
    token: {
        auth: process.env.API_TOKEN_SECRET_AUTH || ''
    },
}

module.exports = DEFAULT
