'use strict'

const dayjs = require('dayjs')
const token = require('./../utils/token')
const Account = require('./../components/account/account')

function authorization(req, res, next) {

    const authorization = req.headers['authorization']

    if (!authorization) {

        res.status(403).send({
            code: 'UNAUTHORIZED_ACCESS'
        })

    } else {

        try {

            const entity = token.decode(authorization)

            if (entity.ex <= dayjs().unix()) {
                res.status(401).send({
                    code: 'EXPIRED_TOKEN',
                    message: 'token has expired.'
                })
            } else {

                Account.findOne({ _id: entity._id }, (error, response) => {
                    if (error) {
                        res.status(500).send({ message: 'Internal Server Error.' })
                    } else {
                        if (!response) {
                            res.status(404).send({
                                code: 'UNAUTHORIZED_ACCESS'
                            })
                        } else {
                            req.account_id = entity._id
                            next()
                        }
                    }
                })

            }



        } catch (error) {
            res.status(401).send({
                code: 'UNAUTHORIZED_ACCESS'
            })
        }

    }

}

module.exports = authorization
