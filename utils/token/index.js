'use strict'

const Jwt = require('jwt-simple')
const dayjs = require('dayjs')
const DEFAULT = require('./../../config/default')

function encode(payload) {

    const t = {
        _id: payload._id,
        name: payload.name,
        surname: payload.surname,
        email: payload.email,
        iat: dayjs().unix(),
        exp: dayjs().add(7, 'days').unix()
    }

    return Jwt.encode(t, DEFAULT.token.auth)

}

function decode(token) {
    const t = token.replace(/["']/g, '');
    return Jwt.decode(t, DEFAULT.token.auth)
}

module.exports = {
    encode, decode
}
