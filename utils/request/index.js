'use strict'

const axios = require('axios')
const DEFAULT = require('./../../config/default')

const client = axios.create({
    baseURL: DEFAULT.endpoint.api
})

const request = function (options) {
    const onSuccess = function (response) {
        return response.data
    }

    const onError = function (error) {
        return Promise.reject(error.response || error.message)
    }

    return client(options)
        .then(onSuccess)
        .catch(onError)
}

module.exports = request
