'use strict'

const ctrl = require('./characterController')

module.exports = {
    list: ctrl.list,
    detail: ctrl.detail
}
