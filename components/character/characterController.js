'use strict'

const { character } = require('./../../services/index')
const FavoriteCtrl = require('./../favorite/favoriteController')

function list(req, res) {

    let page = 1

    if (req.params.page && req.params.page !== 0) {
        page = req.params.page
    }

    character.list({ page }).then(async function (response) {

        const favorites = await FavoriteCtrl.getByAccount(req.account_id)

        if (favorites && favorites.length > 0) {
            if (response.results && response.results.length > 0) {
                response.results.forEach((item) => {
                    const findFavorite = favorites.find(f => f.reference_id === item.id)
                    if (findFavorite) {
                        item.favorite = findFavorite
                    }
                })
            }
        }

        res.status(200).send(response)

    }).catch((err) => {
        res.status(404).send(err)
    })
}

function detail(req, res) {
    if (req.params.id) {
        character.detail(req.params.id).then(async function (response) {
            try {
                const favorite = await FavoriteCtrl.getById(response.id)
                response = { ...response, ...{ favorite } }
            } finally {
                res.status(200).send(response)
            }
        }).catch((err) => {
            res.status(404).send(err.data)
        })
    } else {
        res.status(404).send({ message: 'param {id} is required.' })
    }
}

module.exports = { list, detail }
