'use strict'

const express = require('express')
const controller = require('./index')
const authorization = require('./../../middlewares/authorization')

const api = express.Router()
const character = express.Router()

character.get('/list/:page?', controller.list)
character.get('/detail/:id', controller.detail)

api.use('/character', authorization, character)

module.exports = api

  

