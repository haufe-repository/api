'use strict'

const express = require('express')
const controller = require('./index')
const authorization = require('./../../middlewares/authorization')

const api = express.Router()
const favorite = express.Router()

favorite.post('/save', controller.save)
favorite.delete('/remove', controller.remove)

api.use('/favorite', authorization, favorite)

module.exports = api

  

