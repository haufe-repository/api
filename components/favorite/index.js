'use strict'

const ctrl = require('./favoriteController')

module.exports = {
    save: ctrl.save,
    remove: ctrl.remove
}
