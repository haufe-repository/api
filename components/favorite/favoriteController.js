'use strict'

const Favorite = require('./favorite')

function save(req, res) {

    const params = req.body
    const favorite = new Favorite()

    favorite.account = req.account_id
    favorite.reference_id = params.id

    favorite.save((error, entity) => {

        if (error) {

            res.status(500).send({ message: 'Internal Server Error.' })

        } else {

            if (!entity) {

                res.status(404).send({
                    code: 'FAVORITE_NOT_SAVED',
                    message: `We couldn't save your favorite.`
                })

            } else {

                res.status(200).send(entity)

            }

        }

    })

}

function remove(req, res) {

    if (req.body.id && req.account_id) {

        const id = req.body.id

        Favorite.findOneAndRemove({ reference_id: id, account: req.account_id }, (error, entity) => {

            if (error) {

                res.status(500).send({ message: 'Internal Server Error.' })

            } else {

                if (!entity) {

                    res.status(404).send({
                        code: 'FAVORITE_NOT_REMOVED',
                        message: `We couldn't remove your favorite.`
                    })

                } else {

                    res.status(200).send(entity)

                }

            }

        })

    } else {
        res.status(404).send({ message: 'params {id, accountId} are required.' })
    }

}

function getByAccount(accountId) {
    return new Promise((resolve, reject) => {
        if (accountId) {
            Favorite.find({ account: accountId }, function (error, favorites) {
                if (error) {
                    reject({ message: 'Internal Server Error.' })
                } else {
                    if(!favorites){
                        reject({ message: 'No results found.' })
                    } else {
                        resolve(favorites)
                    }
                }
            })
        } else {
            reject({ message: 'param {accountId} is required.' })
        }
    })
}

function getById(characterId) {
    return new Promise((resolve, reject) => {
        if (characterId) {
            Favorite.findOne({ reference_id: characterId }, function (error, favorite) {
                if (error) {
                    reject({ message: 'Internal Server Error.' })
                } else {
                    if(!favorite){
                        reject({ message: 'No results found.' })
                    } else {
                        resolve(favorite)
                    }
                }
            })
        } else {
            reject({ message: 'param {characterId} is required.' })
        }
    })
}

module.exports = { save, remove, getByAccount, getById }
