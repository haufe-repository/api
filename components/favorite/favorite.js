'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FavoriteSchema = Schema({
    account:                    { type: Schema.ObjectId, ref: 'Account', required: [true, '* Required'] },
    reference_id:               { type: Number, required: [true, '* Required'] },
    created_at:                 { type: Date, default: Date.now }
}, { collection: 'favorite' })

module.exports = mongoose.model('Favorite', FavoriteSchema)
