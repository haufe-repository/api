'use strict'

const express = require('express')
const controller = require('./index')

const api = express.Router()
const account = express.Router()

account.post('/sign-in', controller.signIn)

api.use('/account', account)

module.exports = api

  

