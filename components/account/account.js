'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountSchema = Schema({
    name:                       { type: String, maxlength: 255, trim: true, required: [true, '* Required'] },
    surname:                    { type: String, maxlength: 255, trim: true, required: [true, '* Required'] },
    email:                      {
                                    type: String,
                                    maxlength: 255,
                                    lowercase: true,
                                    trim: true,
                                    unique: true,
                                    required: [true, '* Required'],
                                    validate: {
                                      validator: function(v) {
                                        return /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(v);
                                      },
                                      message: '{VALUE} email is invalid.'
                                    }
                                },
    password:                   { type: String, trim: true, required: [true, '* Required'] },
    created_at:                 { type: Date, default: Date.now },
    updated_at:                 { type: Date, default: Date.now }
}, { collection: 'account' })

module.exports = mongoose.model('Account', AccountSchema)
