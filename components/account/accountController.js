'use strict'

const bcrypt = require('bcrypt')
const Account = require('./account')
const token = require('./../../utils/token')

function signIn(req, res) {

    const params = req.body

    const email = params.email
    const password = params.password

    if (email && password) {

        Account.findOne({ email: email.toLowerCase() }, (error, entity) => {

            if (error) {

                res.status(500).send({ message: 'Internal Server Error.' })

            } else {

                if (!entity) {

                    res.status(404).send({ message: `We couldn't find an account with email ${email}.` })

                } else {

                    bcrypt.compare(password, entity.password, function (err, result) {

                        if (result) {

                            const t = token.encode(entity)

                            res.status(200).send({
                                _id: entity._id,
                                name: entity.name,
                                surname: entity.surname,
                                email: entity.email,
                                token: t
                            })

                        } else {

                            res.status(404).send({ message: 'The username and password not match.' })

                        }

                    })

                }
            }

        })

    } else {

        res.status(404).send({ message: 'params {email, password} are required.' })

    }

}

module.exports = { signIn }
