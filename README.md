## Requirements

1. Node >= 14
2. NPM >= 6
3. Yarn

```
npm install --global yarn
```

## Instructions

1. clone repo

```
git clone git@gitlab.com:haufe-repository/api.git
```

2. cd into directory

```
cd app
```

3. install dependencies using npm or yarn

```
npm install
```

4. create a mongodb dababase

```
haufe_api
```

5. create a mongodb collection

```
account
```

6. insert a mongodb document

```
db.account.insertOne({
    "name" : "Bruce",
    "surname" : "Wayne",
    "email" : "brucewayne@wayneenterprises.com",
    "password" : "$2a$10$3En1SV05bDosGxoX8fR82eYK2AOvAD1lM6UWbaj26OaxqeOfPuBC2",
    "created_at" : ISODate("2021-10-26T14:53:15.936Z"),
    "updated_at" : ISODate("2019-10-26T14:53:15.936Z")
})
```

```
user: brucewayne@wayneenterprises.com
password: ImBatman10
```

7. copy/paste .env.development or .env.production as .env

Inside the project folder, you can run some built-in commands:

### `npm run dev`

Runs the api in development mode.<br>
Open [http://localhost:8978](http://localhost:8978) to view it in the browser.

### Packages

| name                    | description                                                                                   | link                                                                                                         |
| ----------------------- | --------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| cors                    | providing a Connect/Express middleware that can be used to enable CORS with various options.  | [https://www.npmjs.com/package/cors](https://www.npmjs.com/package/cors)                                     |
| helmet                  | helps Express apps by setting various HTTP headers.                                           | [https://www.npmjs.com/package/helmet](https://www.npmjs.com/package/helmet)                                 |
| xss-clean               | Connect middleware to sanitize user input coming from POST body, GET queries, and url params  | [https://www.npmjs.com/package/xss-clean](https://www.npmjs.com/package/xss-clean)                           |
| express-mongo-sanitize  | sanitizes user-supplied data to prevent MongoDB Operator Injection                            | [https://www.npmjs.com/package/express-mongo-sanitize](https://www.npmjs.com/package/express-mongo-sanitize) |

### Routes

| name                     | method                | route                       | params                        | mandatory       |
| ------------------------ | --------------------- | --------------------------- | ----------------------------- |---------------- |
| sign-in                  | POST                  | /account/sign-in            | email, password               | true            |
| character list           | GET                   | /character/list/:page?      | page                          | false           |
| character detail         | GET                   | /character/detail/:id       | id                            | true            |
| favorite save            | POST                  | /favorite/save              | id                            | true            |
| favorite remove          | DELETE                | /favorite/remove            | id                            | true            |

